/**
 * Allow any user with permission.
 */
module.exports = function(req, res, ok) {

  // User is allowed, proceed to controller
  if(typeof req.session.User != 'undefined'){
    if (req.session.User.id) {
      User.findOne(req.session.User.id).exec(function foundUser(err, user){
          var paramsReq = req.url.split('/');
          if(typeof paramsReq[1] !== 'undefined' && typeof paramsReq[2] !== 'undefined'){
            var param2Aux = paramsReq[2].toString();
            if(param2Aux.indexOf('?')!=-1){
              paramsReq[2] = param2Aux.substring(0,param2Aux.indexOf('?'));
            }
            user.hasPermission(paramsReq[1],paramsReq[2],function hasP(err,perm){
              //console.log('Los permisos para la accion: ' + paramsReq[2] + ' en el modelo: ' + paramsReq[1] + ' son: ' + perm);
              if(perm){
                return ok();
              }else{
                return res.forbidden();;
              }
            });
          }else{
            var requireLoginError = [{name: 'requireLogin', message: 'Primero debes loguearte.'}]
            req.session.flash = {
              err: requireLoginError
            }
            res.redirect('/');
            return;
          }
      });
    }
  }else {
    var requireLoginError = [{name: 'requireLogin', message: 'Primero debes loguearte.'}]
    req.session.flash = {
      err: requireLoginError
    }
    res.redirect('/session/new');
    return;
  }
};
