/**
 * GroupController
 *
 * @description :: Server-side logic for managing groups
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {

	new:function(req, res){
		res.view();
	},

	create: function(req, res){
		var groupObj={
			name: req.param('name')
		}
		Group.create(groupObj, function(err,group){
			if(err){
			console.log(err);
				return res.redirect('/group/new');
			}
			res.redirect('/group/index');
		});
	},

	show: function(req, res, next){
		Group.findOne({id:req.param('id')}, function groupFounded(err, group){
			if(err)
				return next(err);
			res.view({
				group: group
			});
		});
	},

	edit: function(req, res, next){
		Group.findOne({id:req.param('id')}, function groupFounded(err, group){
			if(err)
				return next(err);
			if(!group)
				return next();
			res.view({
				group: group
			});
		});
	},

	update: function(req, res, next){
		var groupObj = {
			name: req.param('name')
		}
		Group.update({id:req.param('id')}, groupObj, function GroupUpdate(err, group){
			if(err){
				req.session.flash = {
					err: err
				}
				return res.redirect('/group/edit/'+ req.param('id'));
			}
			res.redirect('/group/show/'+ req.param('id'));
		})
	},

	index: function(req, res, next){
		Group.find().sort('name ASC').exec(function groupFounded(err, group){
			if(err){
				return next(err);
			}
			if(!req.wantsJSON){
				res.view({
					group: group
				});
			}else{
				res.jsonp(group);
			}
		});
	},

	destroy: function(req, res, next){
		Group.destroy({id:req.param('id')}, function groupDestroyed(err){
			if(err){
				return next(err);
			}
			res.redirect('group/index');
		});
	}

};
