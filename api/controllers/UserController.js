/**
 * UserController
 *
 * @description :: Server-side logic for managing users
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {

	new: function(req,res){
		res.view();
	},

	create: function(req, res){
		//creo el objeto con los datos que me pasan en la req
		var groups = req.param('groups');
		var entities = req.param('entities');
		var userObj = {
			name: req.param('name'),
			lastname: req.param('lastname'),
			email: req.param('email'),
			web: req.param('web'),
			birthdate: req.param('birthdate'),
			password: req.param('password'),
			groups:null,
			entities:null
		}
		//pregunto si los grupos tiene algo
		if (typeof groups !== 'undefined' && groups.length>0){
			if (!Array.isArray(groups)){
				//si me pasan un grupo solo lo agrego a un array
				groups = new Array(groups);
			}
			//despues pregunto si en ese array hay douplicado
			//en el caso de haber se dejan solo los unicos
			groups = _.uniq(groups);
			userObj.groups = groups;

		}else{
			//si fallo algo de lo anterior creo el msj con el error
			var groupRequiredError = [{
				name: 'groupRequired',
				message: 'Debe seleccionar algun grupo para el usuario.'
			}]
		}
		//aca se repite lo mismo que para los grupos
		if (typeof entities !== 'undefined' && entities.length>0){
			if (!Array.isArray(entities)){
				entities = new Array(entities);
			}
			entities = _.uniq(entities);
			userObj.entities = entities;
		}else {
			var entityRequiredError = [{
				name: 'entityRequired',
				message: 'Debe seleccionar alguna entidad para el usuario.'
			}]
		}

		if(typeof groupRequiredError !== 'undefined'){
			var error = {
				err: groupRequiredError
			}
		}else if (typeof entityRequiredError !== 'undefined') {
			var error = {
				err: entityRequiredError
			}
		}
		//corroboro si dio un error, en tal caso envio el msj de error
		//junto con los datos que el usuario ya ingreso
		if(typeof error !== 'undefined'){
			return res.view('user/new',{
				user : userObj,
				flash: error
			});
		}
		User.create(userObj, function(err, user){
			if(err){
				var error = User.getValidationMsj(err);
				return res.view('user/new',{
					user : userObj,
					flash: error
				});
			}
			req.session.flash = ({err:[{
				name: 'success',
				message: 'Usuario creado con exito.'
			}]});
			res.redirect('/user/index');
		});
	},

	edit: function(req,res){
		//comprobar que user no sea undefined o que existe
		var idUser = req.param('id');
		if (typeof idUser !== 'undefined'){
			User.findOne({id:idUser}).populate('entities').populate('groups').exec(function(err,usr){
				if(typeof usr !== 'undefined'){
					delete usr.password;
					res.view({
						user: usr
					});
				}else{
					req.session.flash = ({err:[{
						name: 'error',
						message: 'Usuario no existente.'
					}]});
					res.redirect('/user/index');
				}
			});
		}else{
			res.redirect('/user/index');
		}
	},

	show: function(req,res){
		var idUser = req.param('id');
		if (typeof idUser !== 'undefined'){
			User.findOne({id:idUser}).populate('groups').populate('entities').exec(function(err,usr){
				if(typeof usr !== 'undefined'){
					delete usr.password;
					res.view({
						user: usr
					});
				}else{
					req.session.flash = ({err:[{
						name: 'error',
						message: 'Usuario no existente.'
					}]});
					res.redirect('/user/index');
				}
			});
		}else{
			res.redirect('/user/index');
		}
	},

	update: function(req, res){
		//creo el objeto con los datos que me pasan en la req
		var idUser = req.param('id');
		if (typeof idUser !== 'undefined'){
			var idsGroups = req.param('groups');
			var idsEntities = req.param('entities');
			var userObj = {
				id: idUser,
				name: req.param('name'),
				lastname: req.param('lastname'),
				email: req.param('email'),
				web: req.param('web'),
				birthdate: req.param('birthdate'),
				password: req.param('password'),
				groups:null,
				entities:null
			}
			if (userObj.password == '' || typeof userObj.password === 'undefined'){
				userObj.password = 1;
				delete userObj.password;
			}else{
				var nonEncriptedPassword = userObj.password;
				userObj.password = require('bcrypt').hashSync(userObj.password, 10);
			}
			//pregunto si los grupos tiene algo
			if (typeof idsGroups !== 'undefined' && idsGroups.length>0){
				if (!Array.isArray(idsGroups)){
					//si me pasan un grupo solo lo agrego a un array
					idsGroups = new Array(idsGroups);
				}
				//despues pregunto si en ese array hay douplicado
				//en el caso de haber se dejan solo los unicos
				idsGroups = _.uniq(idsGroups);
				for (var i = 0; i < idsGroups.length; i++) {
					userObj.groups = new Array();
					userObj.groups.push({id:idsGroups[i]});
				}

			}else{
				//si fallo algo de lo anterior creo el msj con el error
				var groupRequiredError = [{
					name: 'groupRequired',
					message: 'Debe seleccionar algun grupo para el usuario.'
				}]
			}
			//aca se repite lo mismo que para los grupos
			if (typeof idsEntities !== 'undefined' && idsEntities.length>0){
				if (!Array.isArray(idsEntities)){
					idsEntities = new Array(idsEntities);
				}
				idsEntities = _.uniq(idsEntities);
				for (var i = 0; i < idsEntities.length; i++) {
					userObj.entities = new Array();
					userObj.entities.push({id:idsEntities[i]});
				}
			}else {
				var entityRequiredError = [{
					name: 'entityRequired',
					message: 'Debe seleccionar alguna entidad para el usuario.'
				}]
			}

			if(typeof groupRequiredError !== 'undefined'){
				var error = {
					err: groupRequiredError
				}
			}else if (typeof entityRequiredError !== 'undefined') {
				var error = {
					err: entityRequiredError
				}
			}
			//corroboro si dio un error, en tal caso envio el msj de error
			//junto con los datos que el usuario ya ingreso
			if(typeof error !== 'undefined'){
				if(typeof nonEncriptedPassword !== 'undefined')
					userObj.password = nonEncriptedPassword;
				return res.view('user/edit/',{
					id:idUser,
					user : userObj,
					flash: error
				});
			}
			User.update({id:idUser},userObj, function(err, user){
				if(err){
					var error = User.getValidationMsj(err);
					if(typeof nonEncriptedPassword !== 'undefined')
						userObj.password = nonEncriptedPassword;
					return res.view('user/edit/',{
						id:idUser,
						user : userObj,
						flash: error
					});
				}
				req.session.flash = ({err:[{
					name: 'success',
					message: 'Usuario actualizado con exito.'
				}]});
				res.redirect('/user/show/' + idUser);
			});
		}else {
			res.redirect('/user/index');
		}
	},

	destroy: function(req,res){
		var idUser	= req.param('id');
		User.update({id:idUser},{active:false}).exec(function	(err,uD){
			req.session.flash = ({err:[{
				name: 'success',
				message: 'Usuario deshabilitado con exito.'
			}]});
			res.redirect('/user/index');
		});
	},

	profile: function(req,res,next){
		User.findOne({id:req.session.User.id}, function foundUserLog(err, user){
			res.view({
				user: user
			});
		});
	},

	index: function(req, res, next){
		var pageObj = PaginationService.getCurrentPage(req.param('page'));

		var usrName = req.param('name');
		var usrEmail = req.param('email');
		var active = req.param('active');
		if(active == 'on'){
			active = undefined;
		}else{
			active = true;
		}
		var params={
			skip:pageObj.page,
			limit:pageObj.itemsPerPage
		}
		var cfgParams={
			model: 'user',
			sort: 'name ASC',
			resName: 'users'
		}
		var countParam={};
		if(typeof usrName !== 'undefined') {
			countParam.name = {'contains':usrName};
			params.name = {'contains':usrName};
		}
		if(typeof usrEmail !== 'undefined') {
			countParam.email = {'contains':usrEmail};
			params.email = {'contains':usrEmail};
		}
		if(typeof active !== 'undefined' || active == true) {

			countParam.active = true;
			params.active = true;
		}
		//para solucionar lo del paginado
		//se hace una function recursiva en el modelo
		var idRol = req.param('idRol');
		if(typeof idRol !== 'undefined'){
			var populateParams = new Array();
			populateParams.push({name: 'roles', params: {id:idRol}});
		}
		QueryBuilder.findPaginated(params,countParam,populateParams,req.param('page'),pageObj.itemsPerPage,cfgParams,
		function (err,resp){
			if(err){
				console.log(err);
				res.view('user/index');
			}else{
				if(resp.users.length <= 0){
					var userFindError = [{
						name: 'userFindError',
						message: 'No se encontraron usuarios.'
					}]
					var error = {
						err: userFindError
					}
					resp.flash = error;
				}
				resp.userFindParams = {name: usrName, email:usrEmail, active : active};
				if (req.wantsJSON) {
					res.json(resp);
				}else{
					res.view(resp);
				}
			}
		});
	},

	addToRole: function (req,res,next){
		var idRol = req.param('idRol');
		var idUser = req.param('idUser');
		//console.log('el usuario es: ' + idUser);
		User.findOne(idUser).populate('roles').exec(
				function (err,user){
					user.roles.add(idRol);
					user.save();
					res.jsonp(user);
				}
		);
	},

	removeFromRole: function (req,res,next){
		var idRol = req.param('idRol');
		var idUser = req.param('idUser');
		//console.log('el usuario es: ' + idUser);
		User.findOne(idUser).populate('roles').exec(
				function (err,user){
					user.roles.remove(idRol);
					user.save();
					res.send(200);
				}
		);
	}
};
