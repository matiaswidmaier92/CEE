/**
 * EntityController
 *
 * @description :: Server-side logic for managing entities
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
	new: function(req,res) {
		res.view();
	},
	index: function(req,res){
		var pageObj = PaginationService.getCurrentPage(req.param('page'));
		var entityName = req.param('name');
		var entityEmail = req.param('email');
		var entityDocumentType = req.param('documentType');
		var entityDocument = req.param('document');
		var params={
			skip:pageObj.page,
			limit:pageObj.itemsPerPage
		}
		var cfgParams={
			model: 'entity',
			sort: 'name ASC',
			resName: 'entities'
		}
		var countParam={};
		if(typeof entityName !== 'undefined') {
			countParam.name = {'contains':entityName};
			params.name = {'contains':entityName};
		}
		if(typeof entityEmail !== 'undefined') {
			countParam.email = {'contains':entityEmail};
			params.email = {'contains':entityEmail};
		}
		var populateParams;
		QueryBuilder.findPaginated(params,countParam,populateParams,req.param('page'),pageObj.itemsPerPage,cfgParams,
		function (err,resp){
			if(err){
				console.log(err);
				res.view('entity/index');
			}else{
				if(resp.entities.length <= 0){
					var entityFindError = [{
						name: 'entityFindError',
						message: 'No se encontraron entidades.'
					}]
					var error = {
						err: entityFindError
					}
					resp.flash = error;
				}
				resp.entityFindParams = {name: entityName, email:entityEmail};
				if (req.wantsJSON) {
					res.json(resp);
				}else{
					res.view(resp);
				}
			}
		});
	},

	create: function(req, res){
		if (typeof req.param('name') === 'undefined' || !req.param('name')) {
			var entitynameMismatchError = [{
				name: 'entitynameMismatchError',
				message: 'Debe ingresar un nombre para la entidad.'
			}]
			req.session.flash = {
				err: entitynameMismatchError
			}
			res.redirect('/entity/new');
			return;
		}
		if (typeof req.param('dateOfContract') === 'undefined' || !req.param('dateOfContract')) {
			var entitydateOfContractMismatchError = [{
				name: 'entitydateOfContractMismatchError',
				message: 'Debe ingresar la fecha del contrato con la entidad.'
			}]
			req.session.flash = {
				err: entitydateOfContractMismatchError
			}
			res.redirect('/entity/new');
			return;
		}
		var autoReNew = false;
		if (req.param('autoReNew') == 'on')
			autoReNew = true;
		else
			autoReNew = false;
		var entityData = {
			name: req.param('name'),
			email: req.param('email'),
			web: req.param('web'),
			documentType: req.param('documentType'),
			document: req.param('document')
		}
		var contractData = {
			dateOfContract: req.param('dateOfContract'),
			autoReNew: autoReNew,
			monthToDue: req.param('monthToDue'),
			membership: req.param('membership')
		}
		var data= {entityData:entityData,contractData:contractData}
		Entity.create(entityData, function(err, entity){
			if(err){
				var error = Entity.getValidationMsj(err);
				return res.view('entity/new',{
					flash: error,
					data: data
				});
			}
			Contract.create(contractData, function(err, contract) {
				if(err){
					Entity.destroy({id:entity.id},function(errD){
						var error = Contract.getValidationMsj(err);
						return res.view('entity/new',{
							flash: error,
							data: data
						});
					});
				}else{
					entity.contracts.add(contract);
					entity.save(function (err){
						if(err){
							Entity.destroy({id:entity.id},function(errD){
								Contract.destroy({id:contract.id},function(errD){
									var error = Contract.getValidationMsj(err);
									return res.view('entity/new',{
										flash: error,
										data: data
									});
								});
							});

						}else{
							req.session.flash = ({err:[{
								name: 'success',
								message: 'Entidad creada con exito.'
							}]});
							res.redirect('/entity/index');
						}
					});
				}
			});
		});
	},
	show: function(req,res){
		var id = req.param('id');
		if (typeof id !== 'undefined'){
			Entity.findOne({id:id}).populate('contracts').exec(function entityFounder(err, entity){
				if(err)
					return next(err);
				if(typeof entity !== 'undefined'){
					if (typeof entity.email === 'undefined')
						entity.email = '';
					if (typeof entity.addres === 'undefined')
						entity.addres = '';
					if (typeof entity.web === 'undefined')
						entity.web = '';
					res.view({
						entity: entity
					});
				}else{
					req.session.flash = ({err:[{
						name: 'error',
						message: 'Entidad no existente.'
					}]});
					res.redirect('/entity/index');
				}
			});
		}else{
			res.redirect('/entity/index');
		}

	},
	edit: function(req,res){
		var id = req.param('id');
		if (typeof id !== 'undefined'){
			Entity.findOne({id:req.param('id')}).populate('contracts').exec(function entityFounder(err, entity){
				if(err)
					return next(err);
				if(typeof entity !== 'undefined'){
					if (typeof entity.email === 'undefined')
						entity.email = '';
					if (typeof entity.addres === 'undefined')
						entity.addres = '';
					if (typeof entity.web === 'undefined')
						entity.web = '';
					res.view({
						entity: entity
					})
				}else{
					req.session.flash = ({err:[{
						name: 'error',
						message: 'Entidad no existente.'
					}]});
					res.redirect('/entity/index');
				}
			});
		}else{
			res.redirect('/entity/index');
		}
	},
	delete: function(req,res){

	}
};
