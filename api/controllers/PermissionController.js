/**
 * PermissionController
 *
 * @description :: Server-side logic for managing permissions
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
  new: function(req, res){
  	Model.find().sort('description ASC').exec(function (err,models){
  		Action.find(function (err,actions){
  			res.view({
  				models: models,
  				actions: actions
				});
  		});
		});
	},

	create: function(req, res){
		var pObj = {
			model: req.param('model')
		}
		Permission.create(pObj, function(err, permission){
			if(err){
				var modelRequired = [{
					name: 'modelRequired',
					message: 'Debe seleccionar un modelo para el permiso.'
				}]

			req.session.flash = {
				err: modelRequired
			}
				return res.redirect('/permission/new');
			}
			res.redirect('/permission/edit/' + permission.id)
		})
	},

	index: function(req, res, next){
    var pageObj = PaginationService.getCurrentPage(req.param('page'));

		var modelName = req.param('name');
		var active = req.param('active');
		if(active == 'on'){
			active = undefined;
		}else{
			active = true;
		}
		Model.find({description:{'contains':modelName}}, function(err,models){
			var params={
				skip:pageObj.page,
				limit:pageObj.itemsPerPage
			}
			var cfgParams={
				model: 'permission',
				sort: 'name ASC',
				resName: 'permissions'
			}
			var countParam={};
			if(typeof active !== 'undefined' || active == true) {
				countParam.active = true;
				params.active = true;
			}
			if(typeof modelName !== 'undefined') {
				var auxModels = new Array();
				for (var i = 0; i < models.length; i++) {
					auxModels.push(models[i].id);
				}
				countParam.model = auxModels;
				params.model = auxModels;
			}
			//para solucionar lo del paginado
			//se hace una function recursiva en el modelo
			var populateParams = new Array();
			var idRol = req.param('idRol');
			if(typeof idRol !== 'undefined'){
				var populateParams = new Array();
				populateParams.push({name: 'roles', params: {id:idRol}});
			}
			populateParams.push({name: 'model'});
			QueryBuilder.findPaginated(params,countParam,populateParams,req.param('page'),pageObj.itemsPerPage,cfgParams,function (err,resp){
				if(err){
					console.log(err);
					res.view('permission/index');
				}else{
					if(resp.permissions.length <= 0){
						var permissionFindError = [{
							name: 'permissionFindError',
							message: 'No se encontraron permisos.'
						}]
						var error = {
							err: permissionFindError
						}
						resp.flash = error;
					}
					resp.permissionFindParams = {name: modelName,active:active};
					if (req.wantsJSON) {
						res.json(resp);
					}else{
						res.view(resp);
					}
				}
			});
		});
	},

	edit: function(req,res,next){
		var idPerm = req.param('id');
		if (typeof idPerm !== 'undefined'){
			Permission.findOne({id: idPerm})
				.populate("actions")
				.populate("model")
				.exec(
					function (err, permission){
					if(err)
						return next(err);
					//console.log('Los permisos son: \n' + JSON.stringify(permission));
					//obtener acciones para ese permiso
					if(permission){
						permission.getActionPermission(permission,
						function(err,accionesP){
							//console.log(JSON.stringify(accionesP));
							res.view({
								permission:permission,
								actions:accionesP
							});
						})
					}else{
						req.session.flash = ({err:[{
							name: 'error',
							message: 'Permiso no existente.'
						}]});
						res.redirect('/permission/index');
					}
				}
			);
		}else{
			req.session.flash = ({err:[{
				name: 'error',
				message: 'Permiso no existente.'
			}]});
			res.redirect('/permission/index');
		}
	},

	show: function(req,res,next){
		var idPerm = req.param('id');
		if (typeof idPerm !== 'undefined'){
			Permission.findOne({id: idPerm})
				.populate("actions")
				.populate("model")
				.exec(
					function (err, permission){
					if(err)
						return next(err);
					//console.log('Los permisos son: \n' + JSON.stringify(permission));
					//obtener acciones para ese permiso
					if(permission){
						permission.getActionPermission(permission,
						function(err,accionesP){
							res.view({
								permission:permission,
								actions:accionesP
							});
						})
					}else{
						req.session.flash = ({err:[{
							name: 'error',
							message: 'Permiso no existente.'
						}]});
						res.redirect('/permission/index');
					}
				}
			)
		}else{
			req.session.flash = ({err:[{
				name: 'error',
				message: 'Permiso no existente.'
			}]});
			res.redirect('/permission/index');
		}
	},

/*	update: function(req, res, next){
		var usr;
		if (typeof req.param('users') != 'undefined'){
			usr = req.param('users');
		}
		else{
			usr = null;
		}
		var perm;
		if (typeof req.param('permissions')  != 'undefined'){
			perm = req.param('permissions');
		}
		else{
			perm = null;
		}
		var roleObj = {
			description	: req.param('description'),
			users: usr,
			permissions: perm
		}
		Role.update({id:req.param('id')}, roleObj, function roleUpdate(err, role){
			if(err){
				req.session.flash = Permission.getValidationMsj(err);
				return res.redirect('/role/edit/' + req.param('id'));
			}
			res.redirect('/role/show/' + req.param('id'));
		});
	},*/

	destroy: function	(req,res,next){
		var idPermission	= req.param('id');
    if(idPermission !== 'undefined'){
  		Permission.destroy({id:idPermission}, function	(err,pD){
  			Permission_action.destroy({permission_actions:idPermission}, function (err){
          req.session.flash = ({err:[{
    				name: 'info',
    				message: 'Permiso eliminado con exito.'
    			}]});
  				res.redirect('/permission/index');
  			});
  		});
    }else{
      req.session.flash = ({err:[{
        name: 'error',
        message: 'Debe seleccionar un permiso para eliminar.'
      }]});
      res.redirect('/permission/index');
    }
	},

	getActions: function(req,res,next){
		var idPermission = req.param('permID');
		Permission.findOne({active:true,id:idPermission}).populate('actions').exec(
			function (err,permission){
				if(err)
						return next(err);
				//console.log(idPermission);
				permission.getActionPermission(permission,
					function	(err,acciones){
						res.jsonp(acciones);
					}
				);
			}
		);
	},

	addToRole: function (req,res,next){
		var idRol = req.param('idRol');
		var idPerm = req.param('idPermission');
    if (typeof idRol !== 'undefined' && typeof idPerm !== 'undefined'){
  		Permission.findOne(idPerm).populate('roles').populate('model').exec(
  				function (err,permission){
  					permission.roles.add(idRol);
  					permission.save();
  					res.jsonp(permission);
  				}
  		);
    }else{
      console.log('El idRol o el idPerm estan vacios');
    }
	},

	removeFromRole: function (req,res,next){
		var idRol = req.param('idRol');
		var idPerm = req.param('idPermission');
	   if (typeof idRol !== 'undefined' && typeof idPerm !== 'undefined'){
  		Permission.findOne(idPerm).populate('roles').exec(
  				function (err,permission){
  					permission.roles.remove(idRol);
  					permission.save();
  					res.send(200);
  				}
  		);
    }else{
      console.log('El idRol o el idPerm estan vacios');
    }
	}
};
