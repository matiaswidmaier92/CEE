/**
 * GroupScheduleController
 *
 * @description :: Server-side logic for managing groupschedules
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
	index: function(req,res,next){
		if(!req.wantsJSON){
			var itemsPerPage	= sails.config.pagination.itemsPerPage;
			var page = req.param('page');
			if (typeof page === 'undefined' || !page || page == 1){
				page = 0;	
			}else{
				page = page * itemsPerPage;
				page -= itemsPerPage;
			}

			GroupSchedule.find({active:true, skip:page, limit:itemsPerPage}).exec(function (err, groupschedules){
				GroupSchedule.count().exec( function (errGS,countGroupschedules){
					if(err){
						return next(err);
					}
					var currentPage = req.param('page');
					if (typeof currentPage === 'undefined')
						currentPage = 1;
					res.view({
						currentPage: currentPage,
						cantPag: Math.ceil(countGroupschedules	/ itemsPerPage),
						groupSchedules: groupschedules
					});
				});
			});
		}else{
			var idGSchedule = req.param('idGSchedule');
			GroupSchedule.find({active:true}).exec(
					function (err,groupSchedules){
						
						res.jsonp(groupSchedules);
					}
			);
		}
	}
};

