/**
 * RoleController
 *
 * @description :: Server-side logic for managing roles
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
	new: function(req, res){
		res.view();
	},

	index: function(req, res, next){
		Role.find(function rolesFounded(err, roles){
			if(err){
				return next(err);
			}
			res.view({
				roles: roles
			});
		});
	},

	edit: function(req,res,next){
		Role.findOne({id:req.param('id')}).populate('permissions').populate('users').exec(function roleFounder(err, role){
			if(err)
				return next(err);
			if(!role)
				return next(err);
			
			var permisos = [];
			for (var i = role.permissions.length -1; 	i >= 0; i--) {
					permisos.push(role.permissions[i].id);
			};
			//console.log(permisos);
				Permission.find({where:{id:permisos}})
				.populateAll()
				.exec(function permissionsFounder(err, permissions){
					if(err)
						return next(err);
					//console.log(JSON.stringify(permissions));
					/*var arrPActions = [];
					var accionesP = [];
					console.log('cant perms' + permissions.length);
					async.each(permissions,
						function	(i,callback){
							i.getActionPermission(i, 
							function	(err,accion){
								//arrPActions.push(accion);
								var obj = i.toObject();
					      delete obj.actions;
					      accionesP.push({permission:obj,actions:accion})	 
								//i.actions = {accion;
								
								callback();
							});
							
						},
						function	(err){
							console.log('Res ' + JSON.stringify(accionesP));		
							if(!permissions)
								return next(err);*/
							res.view({
								role: role,
								users: role.users,
								permissions: permissions	
							});
						/*}
					);*/
				});
		});
	},

	show: function(req,res,next){
		Role.findOne({id:req.param('id')}).populate('permissions').populate('users').exec(function roleFounder(err, role){
			if(err)
				return next(err);
			if(!role)
				return next(err);
			
			var permisos = [];
			for (var i = role.permissions.length -1; 	i >= 0; i--) {
					permisos.push(role.permissions[i].id);
			};
				Permission.find({where:{id:permisos}})
				.populateAll()
				.exec(function permissionsFounder(err, permissions){
					if(err)
						return next(err);
							res.view({
								role: role,
								users: role.users,
								permissions: permissions	
							});
				});
		});
	},

	update: function(req, res, next){
		var roleObj = {
			description	: req.param('description')
		}
		Role.update({id:req.param('id')}, roleObj, function roleUpdate(err, role){
			if(err){
				req.session.flash = {
					err : err
				}
				return res.redirect('/role/edit/' + req.param('id'));
			}
			res.redirect('/role/show/' + req.param('id'));
		});
	},

	create: function(req, res){
		var roleObj = {
			description: req.param('description')			
		}
		Role.create(roleObj, function(err, role){
			if(err){
				return res.redirect('/role/new');
			}
			res.redirect('/role/index')
		})
	}
};

