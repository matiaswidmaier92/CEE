/**
 * MaterialController
 *
 * @description :: Server-side logic for managing materials
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {

	new: function(req, res){
		res.view();
	},

	create: function(req, res){
		var materialObj = {
			name: req.param('name'),
			state: req.param('state'),
			inventory: req.param('inventory')
		}
		Material.create(materialObj, function(err, material){
			if(err){
				return res.redirect('/material/new');
			}
			res.redirect('/material/index')
		})
	},

	show: function(req, res, next){
		Material.findOne({id:req.param('id')}, function materialFounder(err, material){
			if(err)
				return next(err);
			res.view({
				material: material
			});
		});
	},

	edit: function(req, res, next){
		Material.findOne({id:req.param('id')}, function materialFound(err, material){
			if(err)
				return next(err);
			if(!material)
				return next();
			res.view({
				material: material
			});
		});
	},

	update: function(req, res, next){
		var materialObj = {
			name: req.param('name'),
			state: req.param('state'),
			inventory: req.param('inventory')
		}
		Material.update({id:req.param('id')}, materialObj, function matreialUpdate(err, material){
			if(err){
				req.session.flash = {
					eerr : err
				}
				return res.redirect('/material/edit/' + req.param('id'));
			}
			res.redirect('/material/show/' + req.param('id'));
		});
	},

	index: function(req, res, next){
		Material.find(function materialFounded(err, material){
			if(err){
				return next(err);
			}
			res.view({
				material: material
			});
		});
	},

	destroy: function(req, res, next){
		Material.destroy({id:req.param('id')}, function materialDestroyed(err){
			if(err){
				return next(err);
			}
			res.redirect('material/index');
		});
	}
};

