/**
 * EventController
 *
 * @description :: Server-side logic for managing events
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {

	new: function(req, res){
		res.view();
	},

	create: function(req, res){
		var eventObj = {
			name: req.param('name'),
			description: req.param('description'),
			quota: req.param('quota')
		}
		Event.create(eventObj, function(err, event){
			if(err){
				return res.redirect('/event/new');
			}
			res.redirect('/event/index')
		})
	},

	show: function(req, res, next){
		Event.findOne({id: req.param('id')}).populate('prices').exec( function eventFounder(err, event){
			if(err)
				return next(err);
			res.view({
				event: event
			});
		});
	},

	edit: function(req, res, next){
		Event.findOne({id:req.param('id')}, function eventFound(err, event){
			if(err)
				return next(err);
			if(!event)
				return next();
			res.view({
				event: event
			});
		});
	},

	update: function(req, res, next){
		var eventObj = {
			name: req.param('name'),
			quota: req.param('quota'),
			description: req.param('description')
		}
		Event.update({id:req.param('id')}, eventObj, function eventUpdate(err, event){
			if(err){
				req.session.flash = {
					eerr : err
				}
				return res.redirect('/event/edit/' + req.param('id'));
			}
			res.redirect('/event/show/' + req.param('id'));
		});
	},

	index: function(req, res, next){
		Event.find(function eventFounded(err, event){
			if(err){
				return next(err);
			}
			res.view({
				event: event
			});
		});
	},

	destroy: function(req, res, next){
		Event.destroy({id:req.param('id')}, function eventDestroyed(err){
			if(err){
				return next(err);
			}
			res.redirect('event/index');
		});
	}


};
