/**
 * OcurrenceController
 *
 * @description :: Server-side logic for managing ocurrences
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {

	new: function(req, res, next){
		Event.find().sort('name ASC').exec(function eventFounded(err,events){
			Group.find().sort('name ASC').exec(function groupFounded (err,groups){
				if(err){
					return next(err);
				}
				if(!req.wantsJSON){
					res.view({events:events,groups:groups});
				}
				else {
					res.jsonp(events, groups);
				}

			});
		});
	},

	create: function(req, res){
		var groups = req.param('groups');
		var users = req.param('users');
		if (typeof groups !== 'undefined' && groups.length>0 || typeof users !== 'undefined' && users.length>0){
			if (!Array.isArray(groups)||!Array.isArray(users)){
				groups = new Array(groups);
				users = new Array(users);
			}
			groups = _.uniq(groups);
			users = _.uniq(users);
			console.log(req.param('idevent'));
			var ocurrenceObj = {
				cost: req.param('cost'),
				sign: req.param('sign'),
				evdate: req.param('evdate'),
				event: req.param('idevent'),
				groups: groups,
				users: users
			}
			Ocurrence.create(ocurrenceObj, function(err, ocurrence){
				if(err){
					console.log(err	);
					return res.redirect('/ocurrence/new');
				}
				res.redirect('/ocurrence/index');
			})
		}else{
			var groupRequiredError = [{
				name: 'groupRequired',
				message: 'Debe seleccionar algun grupo y/o usuario.'
			}]
			req.session.flash = {
				err: groupRequiredError
			}
			return res.redirect('/ocurrence/new');
		}
	},

	edit: function(req, res, next){
		Ocurrence.findOne(req.param('id'), function ocurrenceFound(err, ocurrence){
			if(err)
				return next(err);
			if(!ocurrence)
				return next();
			res.view({
				ocurrence: ocurrence
			});
		});
	},

	show: function(req, res, next){
		Ocurrence.findOne(req.param('id'), function ocurrenceFounder(err, ocurrence){
			if(err)
				return next(err);
			res.view({
				ocurrence: ocurrence
			});
		});
	},

	index: function(req, res, next){
				var itemsPerPage	= sails.config.pagination.itemsPerPage;
				var page = req.param('page');
				if ( page === 'undefined' || !page || page == 1){
					page = 0;
				}else{
					page = page * itemsPerPage;
					page -= itemsPerPage;
				}
				Ocurrence.find({active:true, skip:page, limit:itemsPerPage}).populate('event').exec(function ocurrencesFounded(err, ocurrences){
					Ocurrence.count().exec( function (errCU,countocurrences){
							if(err){
								return next(err);
							}
							var currentPage = req.param('page');
							if (typeof currentPage === 'undefined')
								currentPage = 1;
							res.view({
								currentPage: currentPage,
								cantPag: Math.ceil(countocurrences	/ itemsPerPage),
								ocurrences: ocurrences
							});
						}
					);
				});

		},

		update: function(req, res, next){
			var ocurrenceObj = {
				costo: req.param('cost'),
				moneda: req.param('sign'),
				evdate: req.param('evdate'),
				idevento: req.param('idevent'),
				idevento: req.param('idgroup'),
				idevento: req.param('idperson'),
			}
			Ocurrence.update(req.param('id'), ocurrenceObj, function ocurrenceUpdate(err, ocurrence){
				if(err){
					req.session.flash = {
						err : err
					}
					return res.redirect('/ocurrence/edit/' + req.param('id'));
				}
				res.redirect('/ocurrence/show/' + req.param('id'));
			});
		},

		destroy: function(req, res, next){
			Ocurrence.destroy(req.param('id'), function ocurrenceDestroyed(err){
				if(err){
					return next(err);
				}
				res.redirect('ocurrence/index');
			});
		},

		find: function (req, res, next) {
		var id = req.param('id');
		var idShortCut = isShortcut(id);
		if (idShortCut === true) {
				return next();
		}
		if (id) {
				Ocurrence.findOne(id, function(err, ocurrence) {
						if(ocurrence === undefined) return res.notFound();
						if (err) return next(err);
						res.json(ocurrence);
				});
		} else {
				var where = req.param('where');
				if (_.isString(where)) {
								where = JSON.parse(where);
				}
				// This allows you to put something like id=2 to work.
				// if (!where) {
				//     // Build monolithic parameter object
		 //    params = req.params.all();
		 //    params = _.omit(params, function (param, key) {
		 //        return key === 'limit' || key === 'skip' || key === 'sort'
		 //    });
				//   where = params;
				//   console.log("making it here!");
				// }
				var options = {
										limit: req.param('limit') || undefined,
										skip: req.param('skip')  || undefined,
										sort: req.param('sort') || undefined,
										where: where || undefined
						};
						console.log("Estas son las opciones", options);
				Ocurrence.find(options, function(err, ocurrence) {
						if(ocurrence === undefined) return res.notFound();
						if (err) return next(err);
						res.json(ocurrence);
				});
		}

		function isShortcut(id) {
				if (id === 'find'   ||  id === 'update' ||  id === 'create' ||  id === 'destroy') {
				return true;
				}
		}

		},

};
