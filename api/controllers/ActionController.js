/**
 * ActionController
 *
 * @description :: Server-side logic for managing actions
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
	index: function(req,res,next){
		if(req.wantsJSON){
			var idPerm = req.param('idPermission');
			//console.log(idPerm);
			Action.find({active:true}).populate('permissions',{'permission_actions':idPerm}).exec(function (err,actions){

				return res.jsonp(actions);
			});
		}
	},

	addToPermission: function (req,res,next){
		var idAction = req.param('idAction');
		var idPerm = req.param('idPermission');
		if (typeof idAction !== 'undefined' && typeof idPerm !== 'undefined'){
			Permission_action.create({action_permissions:idAction,
				permission_actions:idPerm,
				canAccess:true}, function (err,pA){
					Action.findOne(idAction).exec(
						function (err,action){
							action.permissions.add(idPerm);
							action.save();
							res.jsonp(action);
						}
					);
				})
			}else{
				console.log('La accion o el permiso no tienen id');
			}
	},

	removeFromPermission: function (req,res,next){
		var idAction = req.param('idAction');
		var idPerm = req.param('idPermission');
		if (typeof idAction !== 'undefined' && typeof idPerm !== 'undefined'){
			Permission_action.destroy({action_permissions:idAction,
				permission_actions:idPerm},function(err,dR){
					res.send(200);
				}
			);
		}else{
			console.log('La accion o el permiso no tienen id');
		}
	}

};
