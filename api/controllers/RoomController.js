/**
 * RoomController
 *
 * @description :: Server-side logic for managing rooms
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {

	new: function(req, res){
		res.view();
	},

	create: function(req, res){
		var roomObj = {
			number: req.param('number'),
			state: req.param('state')
		}
		Room.create(roomObj, function(err, room){
			if(err){
				return res.redirect('/room/new');
			}
			res.redirect('/room/index')
		})
	},

	show: function(req, res, next){
		Room.findOne({id:req.param('id')}, function roomFounder(err, room){
			if(err)
				return next(err);
			res.view({
				room: room
			});
		});
	},

	edit: function(req, res, next){
		Room.findOne({id:req.param('id')}, function roomFound(err, room){
			if(err)
				return next(err);
			if(!room)
				return next();
			res.view({
				room: room
			});
		});
	},

	update: function(req, res, next){
		var roomObj = {
			number: req.param('number'),
			state: req.param('state')
		}
		Room.update({id:req.param('id')}, roomObj, function roomUpdate(err, room){
			if(err){
				req.session.flash = {
					eerr : err
				}
				return res.redirect('/room/edit/' + req.param('id'));
			}
			res.redirect('/room/show/' + req.param('id'));
		});
	},

	index: function(req, res, next){
		Room.find(function roomFounded(err, room){
			if(err){
				return next(err);
			}
			res.view({
				room: room
			});
		});
	},

	destroy: function(req, res, next){
		Room.destroy({id:req.param('id')}, function roomDestroyed(err){
			if(err){
				return next(err);
			}
			res.redirect('room/index');
		});
	}
	
};

