/**
 * ScheduleController
 *
 * @description :: Server-side logic for managing schedules
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
	new: function(req,res){
		res.view();
	},

	edit: function(req,res,next){
		Schedule.findOne({id: req.param('id')})
		.exec(
			function (err, schedule){
			if(err)
				return next(err);
			res.view({
				schedule:schedule
			});
		});
	},

	update: function(req,res,next){
		
	},

	create: function(req,res,next){
		var regExpHour = sails.config.regExp.regExpHour;
		var horaIni = req.param('hourStart');
		if (!regExpHour.test(horaIni)){
			var hourIncorrect = [{
				name: 'invalidHourStart',
				message: 'Debe ingresar una hora de inicio valida.'
			}]
			req.session.flash = {
				err: hourIncorrect
			}

			res.redirect('/schedule/new');
			return;
		}
		var horaFin = req.param('hourEnd');
		if (!regExpHour.test(horaFin)){
			var hourIncorrect = [{
				name: 'invalidHourEnd',
				message: 'Debe ingresar una hora de fin valida.'
			}]
			req.session.flash = {
				err: hourIncorrect
			}

			res.redirect('/schedule/new');
			return;
		}
		var data = {dayOfWeek:req.param('day'),hourStart:horaIni,hourEnd:horaFin}
		Schedule.create(data).exec(function (err,schCreated){
			if(err){
				console.log(err);
				var dayIncorrect = [{
				name: 'invalidDay',
				message: 'Debe ingresar un dia de la semana valido.'
				}]
				req.session.flash = {
					err: dayIncorrect
				}

				res.redirect('/schedule/new');
				return;
			}
			res.redirect('/schedule/index');
		})
	},
	index: function(req,res,next){
		if(!req.wantsJSON){
			var itemsPerPage	= sails.config.pagination.itemsPerPage;
			var page = req.param('page');
			if (typeof page === 'undefined' || !page || page == 1){
				page = 0;	
			}else{
				page = page * itemsPerPage;
				page -= itemsPerPage;
			}

			Schedule.find({active:true, skip:page, limit:itemsPerPage}).exec(function (err, schedules){
				Schedule.count().exec( function (errS,countschedules){
					if(err){
						return next(err);
					}
					var currentPage = req.param('page');
					if (typeof currentPage === 'undefined')
						currentPage = 1;
					res.view({
						currentPage: currentPage,
						cantPag: Math.ceil(countschedules	/ itemsPerPage),
						schedules: schedules
					});
				});
			});
		}else{
			var idGSchedule = req.param('idGSchedule');
			Schedule.find({active:true}).exec(
					function (err,Schedules){
						
						res.jsonp(Schedules);
					}
			);
		}
	}
};

