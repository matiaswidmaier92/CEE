/**
 * ReservationController
 *
 * @description :: Server-side logic for managing reservations
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {

	new: function(req, res){
		Room.find(function (err,rooms){
			Material.find(function (err,materials){
				if(err){
					return next(err);
				}
				res.view({rooms:rooms, materials:materials});
			});		
		});
	},

	create: function(req, res){
		var reservationObj = {
			user: req.param('idUser'),
			date: req.param('date'),
			startTime: req.param('startTime'),
			endTime: req.param('endTime'),
			state: req.param('state'),
			material: req.param('idMaterial'),
			room: req.param('idRoom'),
			staff:req.param('idStaff'),
			ocurrence:req.param('idOcurrence')
		}
		Reservation.create(reservationObj, function(err, reservation){
			if(err){
				return res.redirect('/reservation/new');
			}
			res.redirect('/reservation/index')
		})
	},

	show: function(req, res, next){
		Reservation.findOne(req.param('id'), function reservationFounder(err, reservation){
			if(err)
				return next(err);
			res.view({
				reservation: reservation
			});
		});
	},
	
	edit: function(req, res, next){
		Reservation.findOne(req.param('id'), function reservationFound(err, reservation){
			if(err)
				return next(err);
			if(!reservation)
				return next();
			res.view({
				reservation: reservation
			});
		});
	},

	update: function(req, res, next){
		var reservationObj = {
			user: req.param('idUser'),
			date: req.param('date'),
			startTime: req.param('startTime'),
			endTime: req.param('endTime'),
			state: req.param('state')
		}
		Reservation.update(req.param('id'), reservationObj, function reservationUpdate(err, reservation){
			if(err){
				req.session.flash = {
					eerr : err
				}
				return res.redirect('/reservation/edit/' + req.param('id'));
			}
			res.redirect('/reservarion/show/' + req.param('id'));
		});
	},

	index: function(req, res, next){
		var reservationObj = {
			state: req.param("Confirmado")
		}
		Reservation.find(function reservationFounded(err, reservation){
			if(err){
				return next(err);
			}
			res.view({
				reservation: reservation
			});
		});
	},

	destroy: function(req, res, next){
		Reservation.destroy(req.param('id'), function reservationDestroyed(err){
			if(err){
				return next(err);
			}
			res.redirect('reservation/index');
		});
	},

	confirm: function(req, res, next){
		var reservationObj = {
			idUser: req.param('idUser'),
			idResource: req.param('idResource'),
			date: req.param('date'),
			startTime: req.param('startTime'),
			endTime: req.param('endTime'),
			state: req.param('state')
		}
		Reservation.findOne(req.param('id'), function reservationCon(err, reservation){
			if(err){
				return next(err);
			}else{
				reservation.state = "Confirmado";
				res.redirect('/reservation/index/' + req.param('id') + reservation.state);
			}
		});
	},

	aaaa: function (req, res){
		var reservationObj = {
			state: req.param('state')
		}
		Reservation.update(req.param('id'), reservationObj, function (err, reservation){
			if(err){
				req.session.flash = {
					eerr : err
				}
				return res.redirect('/reservation/index');
			}
			res.redirect('/reservation/index');
		});
	}

};

