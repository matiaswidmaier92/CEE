module.exports= {
  getPaginatedQuery: function(model,params,populateParams,sortStr){
    var model = sails.models[model];
    var modelQuery = model.find(params);
    if (typeof sortStr !== 'undefined' && sortStr != '')
      modelQuery.sort(sortStr);
      if(typeof populateParams !== 'undefined'){
      for (var i = 0; i < populateParams.length; i++) {
        var objPop = populateParams[i];
        if (typeof objPop.params !== 'undefined')
          modelQuery.populate(objPop.name,objPop.params);
        else
          modelQuery.populate(objPop.name);
      }
    }
    return modelQuery;
  },

  findPaginated : function(params,countParam,populateParams,page,itemsPerPage,cfgParams,cb){
    var modelQuery = QueryBuilder.getPaginatedQuery(cfgParams.model,params,populateParams,cfgParams.sort);
    var model = sails.models[cfgParams.model];
    modelQuery.exec(function (err, models){
      model.count(countParam).exec( function (errCU,countModel){
          if(err){
            return cb(err);
          }
          var currentPage = page;
          if (typeof currentPage === 'undefined' || currentPage == ''){
            currentPage = 1;
          }
          if(currentPage > (Math.ceil(countModel	/ itemsPerPage)) && countModel>0){
            currentPage = 1;
            params.skip = 0;
            QueryBuilder.findPaginated(params,countParam,populateParams,currentPage,itemsPerPage,cfgParams,cb);
          }else{
            cb(null,{
              currentPage: currentPage,
              cantPag: Math.ceil(countModel	/ itemsPerPage),
              [cfgParams.resName] : models
            });
          }
        }
      );
    });
  }
}
