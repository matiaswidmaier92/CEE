module.exports = {
	getCurrentPage: function(page){
		var itemsPerPage	= sails.config.pagination.itemsPerPage;
		if ( page === 'undefined' || !page || page == 1){
			page = 0;
		}else{
			page = page * itemsPerPage;
			page -= itemsPerPage;
		}
		var retObj = {page:page,itemsPerPage: itemsPerPage};
		return retObj;
	}
};
