/**
* Contract.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {
	schema: true,
  attributes: {
  	note:{
			type:'string'
		},
  	dateOfContract:{
  		type:'date',
  		required:true
  	},
  	autoReNew:{
  		type:'boolean',
  		defaultsTo:false
  	},
  	monthToDue:{
  		type:'integer',
  		required:true
  	},
		//1: Preincubacion
		//2: Incubacion
		//3: CoWork
		membership:{
			type:'integer',
			enum:[1,2,3],
			required: true
		},
  	entity:{
  		model: 'entity'
  	},
  	staff:{
  		model:'staff'
  	},
		active:{
			type: 'boolean',
			defaultsTo: true
		},
    toJSON: function() {
      var obj = this.toObject();
      delete obj._csrf;
      return obj;
    }
  },

  getValidationMsj: function  (err){
    //validaciones en caso de error
    //por si en caso de que no se cumpla ni uno de los casos
    //envio primero el mensaje en crudo
    var errMsj = [{
      name: 'contractError',
      message:  err.details
    }];
    //aca compruebo si hay errores en c/u de las validaciones
    //en el modelo y en caso de ser se envía un msj de error
    var invAttr = err.invalidAttributes;
    if (typeof invAttr !== 'undefined'){
      var invDateOfContract = invAttr.dateOfContract;
      if(typeof invDateOfContract !== 'undefined'){
        errMsj=new Array();
        for (var i = 0; i < invDateOfContract.length; i++) {
          switch (invDateOfContract[i].rule) {
            case "date":
              errMsj.push({name:'contractError', message: 'Debe ingresar una fecha para el contrato válida.'});
              break;
						case "required":
              errMsj.push({name:'contractError', message: 'Debe ingresar una fecha para el contrato.'});
              break;
          }
        }
      }
      var invMonthToDue = invAttr.monthToDue;
      if(typeof invMonthToDue !== 'undefined'){
        errMsj=new Array();
        for (var i = 0; i < invMonthToDue.length; i++) {
          switch (invMonthToDue[i].rule) {
            case "integer":
              errMsj.push({name:'contractError', message: 'Los meses para el vencimiento deben ser enteros.'});
              break;
						case "required":
							errMsj.push({name:'contractError', message: 'Debe ingresar los meses para el vencimiento.'});
							break;
          }
        }
      }
      var invMemberShip = invAttr.membership;
      if(typeof invMemberShip !== 'undefined'){
        errMsj=new Array();
        for (var i = 0; i < invMemberShip.length; i++) {
          switch (invMemberShip[i].rule) {
            case "in":
              errMsj.push({name:'contractError', message: 'El tipo de mebresía debe estar en la lista.'});
              break;
						case "required":
              errMsj.push({name:'contractError', message: 'Debe ingresar un tipo de mebresía.'});
              break;
          }
        }
      }
    }
    var error = {
      err: errMsj
    }
    return error;
  }
};
