/**
* Material.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {
  schema: true,
  attributes: {
  	name:{
  		type:'string',
  		required:true
  	},

  	inventory:{
  		type:'string',
  		required:true
  	},

    reservations:{
      collection: 'reservation',
      via: 'material'
    },

    active:{
      type:'boolean',
      defaultsTo:true
    },
    toJSON: function() {
      var obj = this.toObject();
      delete obj._csrf;
      return obj;
    }
  }
};

