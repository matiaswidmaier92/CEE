/**
* Entity.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {
	schema: true,
  attributes: {
  	name: {
  		type:'string',
  		required: true,
  		unique: true
  	},
  	email:{
  		type: 'email',
			required: true
  	},
		addres:{
  		type: 'string'
  	},
		documentType:{
  		type: 'integer',
			enum: [1,2,3],
			required: true
  	},
		document:{
			type:'string',
			required: true
		},
  	web:{
  		type: 'string'
  	},
		contracts:{
			collection: 'contract',
			via: 'entity'
		},
		users:{
			collection: 'user',
			via: 'entities'
		},
		active:{
			type: 'boolean',
			defaultsTo: true
		},
    toJSON: function() {
      var obj = this.toObject();
      delete obj._csrf;
      return obj;
    }
  },

  getValidationMsj: function  (err){
    //validaciones en caso de error
    //por si en caso de que no se cumpla ni uno de los casos
    //envio primero el mensaje en crudo
    var errMsj = [{
      name: 'entityError',
      message:  err.details
    }];
    //aca compruebo si hay errores en c/u de las validaciones
    //en el modelo y en caso de ser se envía un msj de error
		var dbError = err.originalError;
    var invAttr = err.invalidAttributes;
    if (typeof invAttr !== 'undefined'){
      var invName = invAttr.name;
      if(typeof invName !== 'undefined'){
        errMsj=new Array();
        for (var i = 0; i < invName.length; i++) {
          switch (invName[i].rule) {
            case "required":
              errMsj.push({name:'entityError', message: 'Se requiere un nombre para la entidad.'});
              break;
						case "unique":
              errMsj.push({name:'entityError', message: 'Ya existe una entidad con ese nombre.'});
              break;
          }
        }
      }
			var invEmail = invAttr.email;
			if(typeof invEmail !== 'undefined'){
        errMsj=new Array();
        for (var i = 0; i < invEmail.length; i++) {
          switch (invEmail[i].rule) {
            case "required":
              errMsj.push({name:'entityError', message: 'Se requiere un email para la entidad.'});
              break;
						case "email":
              errMsj.push({name:'entityError', message: 'Debe ser una direccion de email válida.'});
              break;
          }
        }
      }
			var invDocumentType = invAttr.documentType;
			if(typeof invDocumentType !== 'undefined'){
        errMsj=new Array();
        for (var i = 0; i < invDocumentType.length; i++) {
          switch (invDocumentType[i].rule) {
            case "required":
              errMsj.push({name:'entityError', message: 'Se requiere un tipo de documento.'});
              break;
						case "in":
              errMsj.push({name:'entityError', message: 'El tipo de documento incorrecto.'});
              break;
						case "integer":
              errMsj.push({name:'entityError', message: 'El tipo de documento debe ser entero.'});
              break;
          }
        }
      }
			var invDocument = invAttr.document;
			if(typeof invDocument !== 'undefined'){
        errMsj=new Array();
        for (var i = 0; i < invDocument.length; i++) {
          switch (invDocument[i].rule) {
            case "required":
              errMsj.push({name:'entityError', message: 'Se requiere un documento.'});
              break;
          }
        }
      }
    }else if (typeof dbError !== 'undefined') {
			if(typeof dbError.message !== 'undefined'){
				var errMsj = new Array();
				var message = dbError.message;
				if (message.indexOf('document_documenttype1 dup key') != -1){
					errMsj.push({name:'entityError', message: 'Ya existe una entidad con ese tipo y número de documento.'});
				}
    	}
		}
    var error = {
      err: errMsj
    }
    return error;
  }
};
