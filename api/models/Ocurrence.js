/**
 * Ocurrence.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

 module.exports = {

   attributes: {

     cost : { type: 'float' },

     sign : { type: 'string' },

     evdate : { type: 'date' },

     active:{
       type: 'boolean',
       defaultsTo: true
     },

     event: {
       	model: 'event',
 	      required: true
     	},

     groups: {
       	collection: 'group',
 	      via: 'ocurrence'
     	},

     users: {
       	collection: 'user',
         via: 'ocurrence'
     	}

   }
 };
