/**
* Schedule.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {
  schema: true,
  types: {
  	isHour: function (hour){
  		var regex = sails.config.regExp.regExpHour;
  		return regex.test(hour);
  	}
  },
  attributes: {
  	dayOfWeek:{
  		type: 'string',
  		required: true,
  		enum: ['Lunes','Martes','Miercoles','Jueves','Viernes','Sabado','Domingo']
  	},
  	hourStart: {
  		type: 'string',
  		required:true,
  		isHour: true
  	},
  	hourEnd: {
  		type: 'string',
  		required:true,
  		isHour: true
  	},
  	active: {
  		type: 'boolean',
  		defaultsTo: true
  	},
    toJSON: function() {
      var obj = this.toObject();
      delete obj._csrf;
      return obj;
    }
  },

  beforeValidate: function (values, cb){
    Schedule.find({dayOfWeek: values.dayOfWeek,
      hourStart: values.hourStart,
      hourEnd: values.hourEnd}).exec(
      function (err,resultados){
        console.log(resultados);
        if(!err) cb(err)
        else if(resultados.length != 0){
          console.log('Entra aca');
          cb(new Error ('Ya existe un horario creado con las mismas caracteristicas'));
        }
        else{
          cb();
        }
      }
    );
  }
};
