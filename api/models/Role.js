/**
* Role.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {
  schema: true,
  attributes: {
  	description:{
  		type:'string',
  		required: true,
      unique:true
  	},
  	permissions:{
  		collection: 'permission',
  		via: 'roles'
  	},
  	users:{
  		collection: 'user',
  		via: 'roles'
  	},
    active:{
      type:'boolean',
      defaultsTo:true
    },
    toJSON: function() {
      var obj = this.toObject();
      delete obj._csrf;
      return obj;
    },
    hasPermission: function (model,action,cb){
      var hasPermit = false;
      //obtengo los permisos para el rol
      Role.findOne(this.id).populate('permissions').exec(function getPermRole(err,role){
        //console.log(err);
        //console.log('El role es: ' + JSON.stringify(role.description));
        //console.log('Los permisos para el rol son: ' + JSON.stringify(role.permissions));
        //busco el modelo
        Model.findOne({description:model}).exec(
          function (err,modelo){
            //console.log(err);
            //busco la accion por el nombre

            Action.findOne({name:action}).exec(
              function (err,accion){
                //console.log(err);
                var findPerm = [];
                async.each(role.permissions,
                  function iPerms(iPerm,callback){
                    findPerm.push(iPerm.id);
                    callback();
                  },
                  function endIPerms(err){
                    //console.log(err);
                    if (typeof accion !== 'undefined' && typeof modelo !== 'undefined'){
                      Permission
                        .find({ where: {id: findPerm,model:modelo.id}})
                        .populate('model')
                        .populate('actions',{action_permissions:accion.id})
                        .exec(
                          function getPermModel(err,perm){
                            //console.log(err);
                            var fakeErr = new Error();
                            fakeErr.break = true;
                            async.each(perm,
                              function (iPerm,cbPerm){
                                async.each(iPerm.actions,
                                  function (iAccion,cbAccion){
                                    if(iAccion.canAccess){
                                      hasPermit = true;
                                      cbAccion();
                                    }else{
                                      hasPermit = false;
                                      return cbAccion(fakeErr);
                                    }
                                  },
                                  function (msjEndAccion){
                                    if (typeof msjEndAccion != 'undefined'){
                                      if(msjEndAccion.break) {
                                        return cbPerm(fakeErr);
                                      }
                                    }else{
                                      cbPerm();
                                    }
                                  });
                              },
                              function (msjEndAccion){
                                cb(null,hasPermit);
                              });

                          }
                      );
                    }else{
                      cb(null,hasPermit);
                    }
                  }
                );
              }
            );
          }
        );
      });
    }
  }
};
