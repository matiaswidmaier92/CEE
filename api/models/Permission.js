/**
* Permission.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {
  schema: true,
  attributes: {
  	model:{
  		model:'model',
      required: true
  	},
    actions:{
      collection: 'permission_action',
      references: 'permission_action',
      on: 'permission_actions',
      via: 'action_permissions',
      through: 'permission_action'
    },
  	roles:{
  		collection:'role',
  		via: 'permissions'
  	},
    active:{
      type:'boolean',
      defaultsTo:true
    },
    toJSON: function() {
      var obj = this.toObject();
      delete obj._csrf;
      return obj;
    },

    getActionPermission: function (permission, cb){
      var arrAcciones = [];
        async.each(permission.actions,
          function (iAction,callback){
            arrAcciones.push(iAction.action_permissions);
            callback();
          },
          function (err){
            Action
            .find({where:{id:arrAcciones}})
            .exec(
              function (err,acciones){
                var accionesP = [];
                for (var i = acciones.length - 1; i >= 0; i--) {
                 accionesP.push({'canAccess':permission.actions[i].canAccess, 'action': acciones[i]});
                };

                /*console.log('Y las acciones son: \n' + JSON.stringify(accionesP));
                console.log('Los permisos son: \n' + JSON.stringify(permission));*/
                cb(null,accionesP);
              }
            );
          }
        );
    }
  },

  getValidationMsj: function  (err){
    //validaciones en caso de error
    //por si en caso de que no se cumpla ni uno de los casos
    //envio primero el mensaje en crudo
    var errMsj = [{
      name: 'permissionError',
      message:  err.details
    }];
    //aca compruebo si hay errores en c/u de las validaciones
    //en el modelo y en caso de ser se envía un msj de error
    var invAttr = err.invalidAttributes;
    if (typeof invAttr !== 'undefined'){
      var invModel = invAttr.model;
      if(typeof invModel !== 'undefined'){
        errMsj=new Array();
        for (var i = 0; i < invModel.length; i++) {
          switch (invModel[i].rule) {
            case "required":
              errMsj.push({name:'permissionError', message: 'Se requiere un modelo para el permiso.'});
              break;
          }
        }
      }
    }
    var error = {
      err: errMsj
    }
    return error;
  }
};
