/**
* Event.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {
  schema: true,
  attributes: {
  	name:{
  		type:'string',
  		required:true
  	},

  	description:{
  		type:'string'
  	},

  	active:{
  		type:'boolean',
  		defaultsTo:true
  	},

   ocurrences:{
   	collection: 'ocurrence',
   	via: 'event'
   },

   toJSON: function() {
     var obj = this.toObject();
     delete obj._csrf;
     return obj;
   }
 }
};
