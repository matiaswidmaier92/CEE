/**
* Permission__action.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {
	schema:true,
  attributes: {
  	action_permissions: {
	    columnName: 'action_permissions',
	    type: 'string',
	    foreignKey: true,
	    references: 'action',
	    on: 'id',
	    groupKey: 'action'
	  },
	  permission_actions: {
	    columnName: 'permission_actions',
	    type: 'string',
	    foreignKey: true,
	    references: 'permission',
	    on: 'id',
	    groupKey: 'permission'
	  },
    canAccess:{
      type: 'boolean',
      defaultsTo: false
    },
		active:{
			type:'boolean',
			defaultsTo:true
		},
    toJSON: function() {
      var obj = this.toObject();
      delete obj._csrf;
      return obj;
    }
  }
};