/**
* Reservation.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  attributes: {

  	date:{
  		type:'date',
  		required: true
  	},

  	startTime:{
  		type:'string',
  		required: true
  	},

  	endTime:{
  		type:'string',
  		required: true
  	},

  	state:{
  		type:'string',
  		required:true,
      defaultsTo:'Pendiente'
  	},

    user:{
      model: 'user'
    },

    material:{
      model: 'material'
    },

    room:{
      model: 'room'
    },

    staff:{
      model: 'staff'
    },

    ocurence:{
      model: 'ocurrence'
    }

  }
};

