module.exports = {
  schema: true,
  attributes: {
		name:{
			type:'string',
			required:true
		},
		lastname:{
			type:'string'
		},
		birthdate:{
      required: true,
			type:'date'
		},
		email:{
			type:'email',
			required:true,
			unique: true
		},
		active:{
			type:'boolean',
			defaultsTo:true
		},
		web:{
			type:'string'
		},
		password:{
			type:'string',
			required:true
		},
		roles:{
			collection: 'role',
			via: 'users'
		},
    groups:{
      collection: 'group',
      via: 'users',
      dominant: true
    },
    entities:{
      collection: 'entity',
      via: 'users',
      dominant: true
    },
    ocurrence: {
    	model: 'ocurrence',
  	},

		toJSON: function() {
	      var obj = this.toObject();
	      delete obj.password;
	      delete obj._csrf;
	      return obj;
    },

		hasPermission: function (model,action,cb){
			var hasPermit = false;
			//pregunto para cada rol que tiene el usuario si tiene permisos para
			//realizar las acciones requeridas
			var fakeErr = new Error();
      fakeErr.break = true;
			User.findOne(this.id).populate('roles').exec(function getRolesUsr(err,user){
				//console.log('El largo del areglo roles es : ' + user.roles.length);
				async.each(user.roles,
					function iterator(item,callback){
						item.hasPermission(model,action, function rolP(err,res){
						if(!res) {
							hasPermit=false
							callback(fakeErr);
						}else{
							hasPermit = true;
							callback();
						}
					});
					},
					function end(err){
						cb(null,hasPermit);
					}
				);
			});
		},

		getPermissionsByAction: function(action,cb){
			//pregunto para cada rol que tiene el usuario si tiene permisos para
			//realizar las acciones requeridas
			var fakeErr = new Error();
      fakeErr.break = true;
      var permisosIndex=[];
			User.findOne(this.id).populate('roles').exec(function getRolesUsr(err,user){
				//console.log('El largo del areglo roles es : ' + user.roles.length);
				Model.find(function (err,models){
					async.each(models, function(model,cbModel){
						async.each(user.roles,
							function iterator(item,callback){
								item.hasPermission(model.description,action, function rolP(err,res){
								if(!res) {
									hasPermit=false
									callback(fakeErr);
								}else{
									hasPermit = true;
									callback();
								}
							});
							},
							function end(err){
								permisosIndex.push({model:model.description,action:action,canAccess:hasPermit});
								cbModel();
							}
						);
					},
					function endM(err){
						cb(null,permisosIndex);
					});
				});
			});
		}
  },

	beforeCreate: function (values, next) {
		require('bcrypt').hash(values.password, 10, function passwordEncrypted(err, encryptedPassword) {
		  if (err) return next(err);
		  values.password = encryptedPassword;
		  // values.online= true;
		  next();
		});
	},

  getValidationMsj: function  (err){
    //validaciones en caso de error
    //por si en caso de que no se cumpla ni uno de los casos
    //envio primero el mensaje en crudo
    var errMsj = [{
      name: 'userCreateError',
      message:  err.details
    }];
    //aca compruebo si hay errores en c/u de las validaciones
    //en el modelo y en caso de ser se envía un msj de error
    var invAttr = err.invalidAttributes;
    if (typeof invAttr !== 'undefined'){
      var invEmail = invAttr.email;
      if(typeof invEmail !== 'undefined'){
        errMsj=new Array();
        for (var i = 0; i < invEmail.length; i++) {
          switch (invEmail[i].rule) {
            case "unique":
              errMsj.push({name:'userCreateError', message: 'Email ya existente.'});
              break;
            case "required":
              errMsj.push({name:'userCreateError', message: 'Se requiere un email.'});
              break;
            case "email":
              errMsj.push({name:'userCreateError', message: 'Se requiere un email valido.'});
              break;
          }
        }
      }
      var invName = invAttr.name;
      if(typeof invName !== 'undefined'){
        errMsj=new Array();
        for (var i = 0; i < invName.length; i++) {
          switch (invName[i].rule) {
            case "required":
              errMsj.push({name:'userCreateError', message: 'Se requiere un nombre.'});
              break;
          }
        }
      }
      var invBirthDate = invAttr.birthdate;
      if(typeof invBirthDate !== 'undefined'){
        errMsj=new Array();
        for (var i = 0; i < invBirthDate.length; i++) {
          switch (invBirthDate[i].rule) {
            case "date":
              errMsj.push({name:'userCreateError', message: 'Debe ingresar una fecha de nacimiento valida para el usuario.'});
              break;
            case "required":
              errMsj.push({name:'userCreateError', message: 'Se debe ingresar una fecha de nacimiento.'});
              break;
          }
        }
      }
      var invPassword = invAttr.password;
      if(typeof invPassword !== 'undefined'){
        errMsj=new Array();
        for (var i = 0; i < invPassword.length; i++) {
          switch (invPassword[i].rule) {
            case "required":
              errMsj.push({name:'userCreateError', message: 'Se debe ingresar una contraseña para el usuario.'});
              break;
          }
        }
      }
    }
    var error = {
      err: errMsj
    }
    return error;
  }
};
