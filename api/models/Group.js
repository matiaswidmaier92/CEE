/**
* Group.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {
  schema: true,
  attributes: {
  	name:{
  		type:'string',
  		required:true,
  		unique:true
  	},
		active:{
			type:'boolean',
			defaultsTo:true
		},
    users:{
      collection: 'user',
      via: 'groups'
    },

    ocurrence: {
      	model: 'ocurrence',
    	},

    toJSON: function() {
      var obj = this.toObject();
      delete obj._csrf;
      return obj;
    }
  },
};
