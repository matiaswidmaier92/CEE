/**
 * Bootstrap
 * (sails.config.bootstrap)
 *
 * An asynchronous bootstrap function that runs before your Sails app gets lifted.
 * This gives you an opportunity to set up your data model, run jobs, or perform some special logic.
 *
 * For more information on bootstrapping your app, check out:
 * http://sailsjs.org/#!/documentation/reference/sails.config/sails.config.bootstrap.html
 */

module.exports.bootstrap = function(cb) {
	/*var actionsG=[{name:'all'},{name:'new'},{name:'create'},{name:'edit'},
		{name:'update'},{name:'index'},{name:'destroy'},{name:'profile'},{name:'show'}];
	var users=[{name:'Adminstrador', email: 'admin@cee.com.uy', password: '123456'}, 
		{name:'User', email: 'user@cee.com.uy', password: '123456'}];
	var roles=[{description:'Administrators', users:[]},
		{description:'Users', users:[]}];
	var models=[{description:'Action'},{description:'Event'},{description:'Group'},{description:'Material'},{description:'Model'},
		{description:'Permission'},{description:'Permission_action'},{description:'Role'},{description:'Room'},{description:'User'}];
	

	var createPermissions = function(){

		var permissions=[{model: models[0],roles:roles},{model: models[1],roles:roles},
			{model: models[2],roles:roles},{model: models[3],roles:roles},{model: models[4],roles:roles},
			{model: models[5],roles:roles},{model: models[6],roles:roles},{model: models[7],roles:roles},
			{model: models[8],roles:roles},{model: models[9],roles:roles}];
		console.log('Creando Permisos por defecto');
		Permission.create(permissions).exec(function (err,permisos){
			if(err){
				console.log('Error al crear permisos: ' + err );
				cb();
			}
			else {
				console.log('Se crearon los permisos');
				//console.log(res);
				//delete permiso[0].permissions;

				//permiso[0].actions = ;

				async.each(permisos,
					function iterator(item,callback){
						console.log('Creando relaciones');
						var aux = {
							action_permissions: actionsG[0],							
							permission_actions: item,
							canAccess: true
						}
						Permission_action.create(aux).exec(function (err,permAccion){
							if(err) console.log('Error al crear Permission_action: ' + err);
							else console.log('Se crearon Permission_action');
							aux.action_permissions = actionsG[1];
							Permission_action.create(aux).exec(function (err,permAccion){
								if(err) console.log('Error al crear Permission_action: ' + err);
								else console.log('Se crearon Permission_action');
								aux.action_permissions = actionsG[2];
								Permission_action.create(aux).exec(function (err,permAccion){
									if(err) console.log('Error al crear Permission_action: ' + err);
									else console.log('Se crearon Permission_action');
									aux.action_permissions = actionsG[3];
									Permission_action.create(aux).exec(function (err,permAccion){
										if(err) console.log('Error al crear Permission_action: ' + err);
										else console.log('Se crearon Permission_action');
										aux.action_permissions = actionsG[4];
										Permission_action.create(aux).exec(function (err,permAccion){
											if(err) console.log('Error al crear Permission_action: ' + err);
											else console.log('Se crearon Permission_action');
											aux.action_permissions = actionsG[5];
											Permission_action.create(aux).exec(function (err,permAccion){
												if(err) console.log('Error al crear Permission_action: ' + err);
												else console.log('Se crearon Permission_action');
												aux.action_permissions = actionsG[6];
												Permission_action.create(aux).exec(function (err,permAccion){
													if(err) console.log('Error al crear Permission_action: ' + err);
													else console.log('Se crearon Permission_action');
													aux.action_permissions = actionsG[7];
													Permission_action.create(aux).exec(function (err,permAccion){
														if(err) console.log('Error al crear Permission_action: ' + err);
														else console.log('Se crearon Permission_action');
														aux.action_permissions = actionsG[8];
														Permission_action.create(aux).exec(function (err,permAccion){
															if(err) console.log('Error al crear Permission_action: ' + err);
															else console.log('Se crearon Permission_action');
															callback();
														});
													});
												});
											});
										});
									});
								});			
							});		
						});		

					},
					function end(err){
						cb();
					});
			}			
		});
	}
	
	var createActions = function (){
		console.log('Creando acciones por defecto');
		Action.create(actionsG).exec(function (err,res){
			if(err) console.log('Error al crear acciones: ' + err );
			else {
				actionsG = res;
				console.log('Se crearon las acciones');
			}
			createPermissions();
		});
	};

	var createModels = function(){
		console.log('Creando modelos por defecto');
		Model.create(models).exec(function(err,res){
			if(err) console.log('Error al crear modelos: ' + err );
			else{
				models = res;
				console.log('Se crearon los modelos');
			}

			createActions();
		});
	};

	var createRoles = function(err,users){
		console.log('Creando roles por defecto');
		roles[0].users.push(users[0]);
		roles[1].users.push(users[1]);
		Role.create(roles).exec(function(err,res){
			if(err) console.log('Error al crear roles: ' + err );
			else{
				console.log(JSON.stringify(res));
				roles = res;
				console.log('Se crearon los roles');			
			} 
			createModels();
		});
	};

	console.log('Creando usuarios por defecto');
	User.create(users).exec(function(err,res){
		if(err) console.log('Error al crear usuarios: ' + err );
		else {
			console.log('Se crearon los usuarios');
		}
			createRoles(err,res);
		
	});*/
  // It's very important to trigger this callback method when you are finished
  // with the bootstrap!  (otherwise your server will never lift, since it's waiting on the bootstrap)
  cb();
};
