module.exports.regExp = {
	// [01] son los dig del 0 al 1
	// /d dig del 0 al 9
	// | significa un o si no se cumple lo primero corrobora lo segundo
	// 2[0-3] chequea que si hay un 2 este precedido de los nros del 0 al 3
	regExpHour: /([01]\d|2[0-3]):([0-5]\d)/
};
