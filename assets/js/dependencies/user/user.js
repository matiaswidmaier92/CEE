
$( document ).ready(function(){
	$('#viewModUserDeleteConfirmation').leanModal({
      dismissible: true, // Modal can be dismissed by clicking outside of the modal
      opacity: 0.5, // Opacity of modal background
      in_duration: 300, // Transition in duration
      out_duration: 200,
      ready: function (){

		  }, // Callback for Modal open
      complete: function () {} // Callback for Modal close
    }
  )
	$('#viewModUsers').leanModal({
      dismissible: true, // Modal can be dismissed by clicking outside of the modal
      opacity: 0.5, // Opacity of modal background
      in_duration: 300, // Transition in duration
      out_duration: 200, // Transition out duration
      ready: function (){
				$.ajax({
				  method: "GET",
				  url: "/user/index",
				  dataType: "json"
				})
			  .success(function( users ) {
			  	drawUser.drawTable(users);
			  })
			  .fail(function	(err){
			  	console.log(err);
			  });
		  }, // Callback for Modal open
      complete: function () {drawUser.clearTable();} // Callback for Modal close
    }
  )
})

function openModalDialog(id,email){
  $('#userName').text(email + '?');
  $('#deleteUserForm').attr('action', "/user/destroy/" + id);
}

function eliminarUsuario(){
  $('#deleteUserForm').submit();
}

var drawUser = {
	drawTable: function (data) {
    for (var i = 0; i < data.length; i++) {
      this.drawRow(data[i]);
  	}
	},

	drawRow: function (rowData) {
		var row = $("<tr />")
    $("#tableModalUsers").append(row);
    row.append($("<td>" + rowData.name + "</td>"));
		if($('tr[data-id="' + rowData.id + '"]').length > 0){
			row.append($("<td><button type='button' id='" + rowData.id +
				"' class='btn-floating btn-small waves-effect waves-light red btnAction' onClick=\"drawUser.destroy('"+rowData.id+"')\"><i class='material-icons'>remove</i></button></td>"));
		}else	{
			row.append($("<td><button type='button' id='" + rowData.id +
				"' class='btn-floating btn-small waves-effect waves-light green btnAction' onClick=\"drawUser.addToTable({id:'"+rowData.id+"'})\"><i class='material-icons'>add</i></button></td>"));
		}
	},

	clearTable: function (){
		$("#tableModalUsers tbody tr").remove();
	},

	addToTable: function(user) {

	  var obj = {
	    user: user,
	    _csrf: window.cee.csrf || ''
	  };
		$('.btnAction').prop('disabled', false);
	  $("#" + user.id).replaceWith("<button type='button' id='" + user.id +
    		"' class='btn-floating btn-small waves-effect waves-light red btnAction' onClick=\"drawUser.destroy('"+user.id+"')\"><i class='material-icons'>remove</i></button>");

	  $( '#tableUsers tr:last' ).after(

      JST['assets/templates/newUserTo.ejs']( obj )
    );
  },

  destroy: function(id) {
  	$('.btnAction').prop('disabled', false);
  	$("#" + id).replaceWith("<button type='button' id='" + id +
				"' class='btn-floating btn-small waves-effect waves-light green btnAction' onClick=\"drawUser.addToTable({id:'"+id+"'})\"><i class='material-icons'>add</i></button>");
    $('tr[data-id="' + id + '"]').remove();
  }
}
