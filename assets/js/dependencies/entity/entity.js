$( document ).ready(function(){
	$('#viewModEntities').leanModal({
      dismissible: true, // Modal can be dismissed by clicking outside of the modal
      opacity: 0.5, // Opacity of modal background
      in_duration: 300, // Transition in duration
      out_duration: 200, // Transition out duration
      ready: function (){
				$.ajax({
				  method: "GET",
				  url: "/entity/index",
				  dataType: "json"
				})
			  .success(function( entitys ) {
			  	drawEntity.drawTable(entitys);
			  })
			  .fail(function	(err){
			  	console.log(err);
			  });
		  }, // Callback for Modal open
      complete: function () {drawEntity.clearTable();} // Callback for Modal close
    }
  )
})

$( document ).ready(function(){
	$('#viewModEntitiesShow').leanModal({
      dismissible: true, // Modal can be dismissed by clicking outside of the modal
      opacity: 0.5, // Opacity of modal background
      in_duration: 300, // Transition in duration
      out_duration: 200
    }
  )
})

var drawEntity = {
	drawTable: function (data) {
    for (var i = 0; i < data.length; i++) {
      this.drawRow(data[i]);
  	}
	},

	drawRow: function (rowData) {
		var row = $("<tr />")
    $("#tableModalEntities").append(row);
    row.append($("<td>" + rowData.name + "</td>"));
		if($('tr[data-id="' + rowData.id + '"]').length > 0){
			row.append($("<td><button type='button' id='btn" + rowData.id +
				"' class='btn-floating btn-small waves-effect waves-light red btnAction' onClick=\"drawEntity.destroy('"+rowData.id+"')\"><i class='material-icons'>remove</i></button></td>"));
		}else	{
			row.append($("<td><button type='button' id='btn" + rowData.id +
				"' class='btn-floating btn-small waves-effect waves-light green btnAction' onClick=\"drawEntity.addToTable({id:'"+rowData.id+"'})\"><i class='material-icons'>add</i></button></td>"));
		}
	},

	clearTable: function (){
		$("#tableModalEntities tbody tr").remove();
	},

	addToTable: function(entity) {

	  var obj = {
	    entity: entity,
	    _csrf: window.cee.csrf || ''
	  };
		$('.btnAction').prop('disabled', false);
	  $("#btn" + entity.id).replaceWith("<button type='button' id='btn" + entity.id +
    		"' class='btn-floating btn-small waves-effect waves-light red btnAction' onClick=\"drawEntity.destroy('"+entity.id+"')\"><i class='material-icons'>remove</i></button>");

	  $( '#tableEntities tr:last' ).after(

      JST['assets/templates/newEntityToUser.ejs']( obj )
    );
  },

  destroy: function(id) {
  	$('.btnAction').prop('disabled', false);
  	$("#btn" + id).replaceWith("<button type='button' id='btn" + id +
				"' class='btn-floating btn-small waves-effect waves-light green btnAction' onClick=\"drawEntity.addToTable({id:'"+id+"'})\"><i class='material-icons'>add</i></button>");
    $('tr[data-id="' + id + '"]').remove();
  }
}
