$( document ).ready(function(){
  $(".button-collapse").sideNav();
 	$('select').material_select();
 	$('.datepicker').pickadate({
  	selectMonths: true, // Creates a dropdown to control month
  	selectYears: 15 // Creates a dropdown of 15 years to control year
  });

  $('.collapsible').collapsible({
    accordion : false // A setting that changes the collapsible behavior to expandable instead of the default accordion style
  });

  $('.pageButton').on('click', function(){
    var page = $(this).attr("id");
    var p = $('#page');
    $('#page').val(page);
    $('#find').submit();
  });

  var $input = $('.datepicker').pickadate();
  // Use the picker object directly.
  var picker = $input.pickadate('picker');
  if(typeof picker !== 'undefined'){
    var stringDate = $('#dateToSet').val();
    if(stringDate != ''){
      var fecha = new Date(stringDate);
    }else {
      var fecha = new Date();
    }

    picker.set('select', fecha);

  }
})

function clearFindForm(){
  $(':input','#find') .not(':button, :submit, :reset, :hidden')
  .val('')
  .removeAttr('checked')
  .removeAttr('selected');
  $('[name=active]').removeAttr('checked')
  $('#find').submit();
}

var delay = (function(){
  var timer = 0;
    return function(callback, ms){
    	clearTimeout (timer);
    	timer = setTimeout(callback, ms);
    };
})();
