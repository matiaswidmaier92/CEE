$( document ).ready(function(){
	$('#openModUsers').leanModal({
      dismissible: true, // Modal can be dismissed by clicking outside of the modal
      opacity: .5, // Opacity of modal background
      in_duration: 300, // Transition in duration
      out_duration: 200, // Transition out duration
      ready:function(){ ajaxReqIndexUsuarios();}, // Callback for Modal open
      complete: function () {drawUserToRole.clearTable();} // Callback for Modal close
    }
  )

  $('#openModPermissions').leanModal({
      dismissible: true, // Modal can be dismissed by clicking outside of the modal
      opacity: .5, // Opacity of modal background
      in_duration: 300, // Transition in duration
      out_duration: 200, // Transition out duration
      ready:function(){ ajaxReqIndexPermisos();}, // Callback for Modal open
      complete: function () {drawPermissionsToRole.clearTable();} // Callback for Modal close
    }
  )
	var userName;
	var email;
	var page;
	$('.modalFilterUsers').on('keyup',function(){
		if(userName != $('[name=userName]').val() || email != $('[name=email]').val() || page != $('[name=page]').val()){
			delay(function(){
				drawUserToRole.clearTable();
				ajaxReqIndexUsuarios();
				userName = $('[name=userName]').val();
				email = $('[name=email]').val();
				page = $('[name=page]').val();
			}, 250);
		}
	});

	var model;
	var page;
	$('.modalFilterPermissions').on('keyup',function(){
		if(model != $('[name=modelName]').val() || page != $('[name=page]').val()){
			delay(function(){
				drawPermissionsToRole.clearTable();
				ajaxReqIndexPermisos();
				model = $('[name=modelName]').val();
				page = $('[name=page]').val();
			}, 250);
		}
	});
})

function ajaxReqIndexUsuarios(){
	var userName = $('[name=userName].modalFilterUsers').val();
	var email = $('[name=email].modalFilterUsers').val();
	var page = $('[name=page].modalFilterUsers').val();
	$.ajax({
		method: "GET",
		url: "/user/index",
		data: { idRol: $("#idRol").val(),
		name: userName,
		email: email,
		page: page},
		dataType: "json"
	})
	.success(function( resp ) {
		drawUserToRole.drawTable(resp);
	})
	.fail(function	(err){
		console.log(err);
	});
}

function ajaxReqIndexPermisos(){
	var modelName = $('[name=modelName].modalFilterPermissions').val();
	var page = $('[name=page].modalFilterPermissions').val();
	$.ajax({
		method: "GET",
		url: "/permission/index",
		data: { idRol: $("#idRol").val(),
		name: modelName,
		page: page},
		dataType: "json"
	})
	.success(function( resp ) {
		drawPermissionsToRole.drawTable(resp);
	})
	.fail(function	(err){
		console.log(err);
	});
}

function obtenerInfoAcciones(idP){
	$.ajax({
		method:'POST',
		'url':'/permission/getActions',
		data:{_csrf: window.cee.csrf || '',permID:idP}
	})
	.success(function( actions ) {
		drawPermissionsToRole.drawActionsTable(actions);
		$('#viewDetailPermission').openModal();
  })
  .fail(function	(err){
  	console.log(err);
  });
}

//ajax para agregar un usuario al rol
function addUserToRole(usrID){
	$('.btnAction').prop('disabled', true);
	$.ajax({
		method:'POST',
		'url':'/user/addToRole',
		data:{_csrf: window.cee.csrf || '', idRol:$("#idRol").val(),idUser:usrID}
	})
	.success(function( user ) {
		RoleIndexPage.addUserToTable(user);
	})
	.fail(function	(err){
		console.log(err);
	});
}

//ajax para remover un usuario del rol
function removeUserFromRole(usrID){
	$('.btnAction').prop('disabled', true);
	$.ajax({
	 		method:'POST',
	 		'url':'/user/removeFromRole',
	 		data:{_csrf: window.cee.csrf || '', idRol:$("#idRol").val(),idUser:usrID}
	 })
	 .success(RoleIndexPage.destroyUser(usrID))
	 .fail(function	(err){
		console.log(err);
	 });
}

//ajax para agregar un permiso al rol
function addPermissionToRole(permID){
	$('.btnAction').prop('disabled', true);
	$.ajax({
		method:'POST',
		'url':'/permission/addToRole',
		data:{_csrf: window.cee.csrf || '', idRol:$("#idRol").val(),idPermission:permID}
	})
	.success(function( permiso ) {
		RoleIndexPage.addPermissionToTable(permiso);
	})
	.fail(function	(err){
		console.log(err);
	});
}

function changePageNumberToModalUsers(nro){
	delay(function(){
		$('#page.modalPaginatedUsers').val(nro);
		drawUserToRole.clearTable();
		ajaxReqIndexUsuarios();
	}, 250);
}

function changePageNumberToModalPermissions(nro){
	delay(function(){
		$('#page.modalPaginatedPermissions').val(nro);
		drawPermissionsToRole.clearTable();
		ajaxReqIndexPermisos();
	}, 250);
}
//ajax para remover un permiso del rol
function removePermissionFromRole(permID){
	$('.btnAction').prop('disabled', true);
	$.ajax({
	 		method:'POST',
	 		'url':'/permission/removeFromRole',
	 		data:{_csrf: window.cee.csrf || '', idRol:$("#idRol").val(),idPermission:permID}
	 })
	 .success(RoleIndexPage.destroyPermission(permID))
	 .fail(function	(err){
		console.log(err);
	 });
}

//dibujado de permisos en su respectiva tabla
var drawPermissionsToRole = {

	drawActionsTable: function (data){
		drawPermissionsToRole.clearTableAction();
		for (var i = 0; i < data.length; i++) {
      this.drawActionRow(data[i]);
    }
		var mainDiv = $("#mainModalPermissions");
		$("#modalPaginationPermissions").remove();
		var htmlToAppend;
		htmlToAppend = "<ul class='pagination' id='modalPaginationPermissions' style='margin: 6px 0;'>"

		for (var i = 1; i <= data.cantPag; i++) {
			if(data.currentPage == i || data.currentPage == ''){
				htmlToAppend += "<li class='active'><a class='pageButton' id='" + i + "' href=\"javascript:changePageNumberToModalPermissions('" + i + "')\">" + i + "</a></li>";
			}else{
				htmlToAppend += "<li><a class='pageButton' id='" + i + "' href=\"javascript:changePageNumberToModalPermissions('" + i + "')\">" + i + "</a></li>";
			}
		}
		htmlToAppend += "</ul>";
		mainDiv.append(htmlToAppend);
	},

	drawActionRow: function(rowData){
		var row = $("<tr />")
    $("#modalAcctions").append(row);
    row.append($("<td>" + rowData.action.name + "</td>"));
    row.append($("<td>" + rowData.canAccess + "</td>"));
    /*if(rowData.roles.length != 0){
    	row.append($("<td><button type='button' id='" + rowData.id +
    		"' class='btn btn-danger btnAction' onClick=\"removePermissionFromRole('"+rowData.id+"')\">Remover</button></td>"));
    }else	{
			row.append($("<td><button type='button' id=" + rowData.id +
				" class='btn btn-success btnAction' onClick=\"addPermissionToRole('"+rowData.id+"')\">Agregar</button></td>"));
    }*/
	},

	clearTableAction: function (){
		$("#modalAcctions tbody tr").remove();
	},

	clearTable: function (){
		$("#modalPermissions tbody tr").remove();
	},

	drawTable: function (data) {
		drawPermissionsToRole.clearTableAction();
    for (var i = 0; i < data.permissions.length; i++) {
      this.drawRow(data.permissions[i]);
    }
		var mainDiv = $("#mainModalPermissions");
		$("#modalPaginationPermissions").remove();
		var htmlToAppend;
		htmlToAppend = "<ul class='pagination' id='modalPaginationPermissions' style='margin: 6px 0;'>"

		for (var i = 1; i <= data.cantPag; i++) {
			if(data.currentPage == i || data.currentPage == ''){
				htmlToAppend += "<li class='active'><a class='pageButton' id='" + i + "' href=\"javascript:changePageNumberToModalPermissions('" + i + "')\">" + i + "</a></li>";
			}else{
				htmlToAppend += "<li><a class='pageButton' id='" + i + "' href=\"javascript:changePageNumberToModalPermissions('" + i + "')\">" + i + "</a></li>";
			}
		}
		htmlToAppend += "</ul>";
		mainDiv.append(htmlToAppend);
	},

	drawRow: function (rowData) {
    var row = $("<tr />")
    $("#modalPermissions").append(row);
    row.append($("<td>" + rowData.id + "</td>"));
    row.append($("<td>" + rowData.model.description + "</td>"));
    if(rowData.roles.length != 0){
    	row.append($("<td><button type='button' id='" + rowData.id +
    		"' class='btn-floating btn-small waves-effect waves-light red btnAction' onClick=\"removePermissionFromRole('"+rowData.id+"')\"><i class='material-icons'>remove</i></button></td>"));
    }else	{
			row.append($("<td><button type='button' id='" + rowData.id +
				"' class='btn-floating btn-small waves-effect waves-light green btnAction' onClick=\"addPermissionToRole('"+rowData.id+"')\"><i class='material-icons'>add</i></button></td>"));
    }
	}
}

//dibujado de usuarios en su respectiva tabla
var drawUserToRole = {
	drawTable: function (data) {
		var row;
    for (var i = 0; i < data.users.length; i++) {
      this.drawRow(data.users[i]);
  	}
		var mainDiv = $("#mainModalUsers");
		$("#modalPaginationUsers").remove();
		var htmlToAppend;
		htmlToAppend = "<ul class='pagination' id='modalPaginationUsers' style='margin: 6px 0;'>"

		for (var i = 1; i <= data.cantPag; i++) {
			if(data.currentPage == i || data.currentPage == ''){
				htmlToAppend += "<li class='active'><a class='pageButton' id='" + i + "' href=\"javascript:changePageNumberToModalUsers('" + i + "')\">" + i + "</a></li>";
			}else{
				htmlToAppend += "<li><a class='pageButton' id='" + i + "' href=\"javascript:changePageNumberToModalUsers('" + i + "')\">" + i + "</a></li>";
			}
		}
		htmlToAppend += "</ul>";
		mainDiv.append(htmlToAppend);
	},

	drawRow: function (rowData) {
    var row = $("<tr />")
    $("#modalUsers").append(row);
    row.append($("<td>" + rowData.name + "</td>"));
    row.append($("<td>" + rowData.email + "</td>"));
    if(typeof rowData.web != 'undefined') row.append($("<td>" + rowData.web + "</td>"));
    else row.append($("<td></td>"));
    if(rowData.roles.length != 0){
    	row.append($("<td><button type='button' id='" + rowData.id +
    		"' class='btn-floating btn-small waves-effect waves-light red btnAction' onClick=\"removeUserFromRole('"+rowData.id+"')\"><i class='material-icons'>remove</i></button></td>"));
    }else	{
			row.append($("<td><button type='button' id='" + rowData.id +
				"' class='btn-floating btn-small waves-effect waves-light green btnAction' onClick=\"addUserToRole('"+rowData.id+"')\"><i class='material-icons'>add</i></button></td>"));
    }
	},

	clearTable: function (){
		$("#modalUsers tbody tr").remove();
	}

}

//funciones para actualizar la lista de usuarios del rol
var RoleIndexPage = {

  addUserToTable: function(user) {

	  var obj = {
	    user: user,
	    _csrf: window.cee.csrf || ''
	  };
	  $('.btnAction').prop('disabled', false);
	  $("#" + user.id).replaceWith("<button type='button' id='" + user.id +
    		"' class='btn-floating btn-small waves-effect waves-light red btnAction' onClick=\"removeUserFromRole('"+user.id+"')\"><i class='material-icons'>remove</i></button>");

    $( '#tablaUserRol tr:last' ).after(

      JST['assets/templates/newUserToRole.ejs']( obj )
    );
  },

  destroyUser: function(id) {
  	$('.btnAction').prop('disabled', false);
  	$("#" + id).replaceWith("<button type='button' id='" + id +
				"' class='btn-floating btn-small waves-effect waves-light green btnAction' onClick=\"addUserToRole('"+id+"')\"><i class='material-icons'>add</i></button>");
    $('tr[data-id="' + id + '"]').remove();
  },

  addPermissionToTable: function(permission) {

	  var obj = {
	    permission: permission,
	    _csrf: window.cee.csrf || ''
	  };
	  $('.btnAction').prop('disabled', false);
	  $("#" + permission.id).replaceWith("<button type='button' id='" + permission.id +
    		"' class='btn-floating btn-small waves-effect waves-light red btnAction' onClick=\"removePermissionFromRole('"+permission.id+"')\"><i class='material-icons'>remove</i></button>");

    $( '#tablaPermissionRol tr:last' ).after(

      JST['assets/templates/newPermissionToRole.ejs']( obj )
    );
  },

  destroyPermission: function(id) {
  	$('.btnAction').prop('disabled', false);
  	$("#" + id).replaceWith("<button type='button' id='" + id +
				"' class='btn-floating btn-small waves-effect waves-light green btnAction' onClick=\"addPermissionToRole('"+id+"')\"><i class='material-icons'>add</i></button>");
    $('tr[data-id="' + id + '"]').remove();
  }
}
