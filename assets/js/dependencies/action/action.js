$( document ).ready(function(){
	$('#viewModActionsP').leanModal({
      dismissible: true, // Modal can be dismissed by clicking outside of the modal
      opacity: 0.5, // Opacity of modal background
      in_duration: 300, // Transition in duration
      out_duration: 200, // Transition out duration
      ready: function (){
				$.ajax({
				  method: "GET",
				  url: "/action/index",
				  data: {idPermission:$("#idPermission").val()},
				  dataType: "json"
				})
			  .success(function( actions ) {
			  	drawActionsP.drawTable(actions);
			  })
			  .fail(function	(err){
			  	console.log(err);
			  });
		  }, // Callback for Modal open
      complete: function () {drawActionsP.clearTable();} // Callback for Modal close
    }
  )
})

//ajax para agregar un accion al permiso
function addActionToPermission(actID){
	$('.btnAction').prop('disabled', true);
	$.ajax({
		method:'POST',
		'url':'/action/addToPermission',
		data:{_csrf: window.cee.csrf || '', idPermission:$("#idPermission").val(),idAction:actID}
	})
	.success(function( accion ) {
		drawActionsP.addToTable(accion);
	})
	.fail(function	(err){
		console.log(err);
	});
}

//ajax para remover un accion del permiso
function removeActionFromPermission(actID){
	$('.btnAction').prop('disabled', true);
	$.ajax({
	 		method:'POST',
	 		'url':'/action/removeFromPermission',
	 		data:{_csrf: window.cee.csrf || '', idPermission:$("#idPermission").val(),idAction:actID}
	 })
	 .success(drawActionsP.destroy(actID))
	 .fail(function	(err){
		console.log(err);
	 });
}

var drawActionsP = {
	drawTable: function (data) {
    for (var i = 0; i < data.length; i++) {
      this.drawRow(data[i]);
  	}
	},

	drawRow: function (rowData) {
		var row = $("<tr />")
    $("#modalAcctionsP").append(row);
    row.append($("<td>" + rowData.name + "</td>"));
    if(rowData.permissions.length != 0){
    	row.append($("<td><button type='button' id='" + rowData.id +
    		"' class='btn-floating btn-small waves-effect waves-light red btnAction' onClick=\"removeActionFromPermission('"+rowData.id+"')\"><i class='material-icons'>remove</i></button></td>"));
    }else	{
			row.append($("<td><button type='button' id='" + rowData.id +
				"' class='btn-floating btn-small waves-effect waves-light green btnAction' onClick=\"addActionToPermission('"+rowData.id+"')\"><i class='material-icons'>add</i></button></td>"));
    }
	},

	clearTable: function (){
		$("#modalAcctionsP tbody tr").remove();
	},

	addToTable: function(action) {

	  var obj = {
	    action: action,
	    _csrf: window.cee.csrf || ''
	  };
	  $('.btnAction').prop('disabled', false);
	  $("#" + action.id).replaceWith("<button type='button' id='" + action.id +
    		"' class='btn-floating btn-small waves-effect waves-light red btnAction' onClick=\"removeActionFromPermission('"+action.id+"')\"><i class='material-icons'>remove</i></button>");

    $( '#tablaAccionesPermiso tr:last' ).after(

      JST['assets/templates/newActionToPermission.ejs']( obj )
    );
  },

  destroy: function(id) {
  	$('.btnAction').prop('disabled', false);
  	$("#" + id).replaceWith("<button type='button' id='" + id +
				"' class='btn-floating btn-small waves-effect waves-light green btnAction' onClick=\"addActionToPermission('"+id+"')\"><i class='material-icons'>add</i></button>");
    $('tr[data-id="' + id + '"]').remove();
  }
}
