$( document ).ready(function(){
	$('#viewModGroups').leanModal({
      dismissible: true, // Modal can be dismissed by clicking outside of the modal
      opacity: 0.5, // Opacity of modal background
      in_duration: 300, // Transition in duration
      out_duration: 200, // Transition out duration
      ready: function (){
				$.ajax({
				  method: "GET",
				  url: "/group/index",
				  dataType: "json"
				})
			  .success(function( groups ) {
			  	drawGroup.drawTable(groups);
			  })
			  .fail(function	(err){
			  	console.log(err);
			  });
		  }, // Callback for Modal open
      complete: function () {drawGroup.clearTable();} // Callback for Modal close
    }
  )
})

$( document ).ready(function(){
	$('#viewModGroupsShow').leanModal({
      dismissible: true, // Modal can be dismissed by clicking outside of the modal
      opacity: 0.5, // Opacity of modal background
      in_duration: 300, // Transition in duration
      out_duration: 200
    }
  )
})

var drawGroup = {
	drawTable: function (data) {
    for (var i = 0; i < data.length; i++) {
      this.drawRow(data[i]);
  	}
	},

	drawRow: function (rowData) {
		var row = $("<tr />")
    $("#tableModalGroups").append(row);
    row.append($("<td>" + rowData.name + "</td>"));
		if($('tr[data-id="' + rowData.id + '"]').length > 0){
			row.append($("<td><button type='button' id='btn" + rowData.id +
				"' class='btn-floating btn-small waves-effect waves-light red btnAction' onClick=\"drawGroup.destroy('"+rowData.id+"')\"><i class='material-icons'>remove</i></button></td>"));
		}else	{
			row.append($("<td><button type='button' id='btn" + rowData.id +
				"' class='btn-floating btn-small waves-effect waves-light green btnAction' onClick=\"drawGroup.addToTable({id:'"+rowData.id+"'})\"><i class='material-icons'>add</i></button></td>"));
		}
	},

	clearTable: function (){
		$("#tableModalGroups tbody tr").remove();
	},

	addToTable: function(group) {

	  var obj = {
	    group: group,
	    _csrf: window.cee.csrf || ''
	  };
		$('.btnAction').prop('disabled', false);
	  $("#btn" + group.id).replaceWith("<button type='button' id='btn" + group.id +
    		"' class='btn-floating btn-small waves-effect waves-light red btnAction' onClick=\"drawGroup.destroy('"+group.id+"')\"><i class='material-icons'>remove</i></button>");

	  $( '#tableGroups tr:last' ).after(

      JST['assets/templates/newGroupTo.ejs']( obj )
    );
  },

  destroy: function(id) {
  	$('.btnAction').prop('disabled', false);
  	$("#btn" + id).replaceWith("<button type='button' id='btn" + id +
				"' class='btn-floating btn-small waves-effect waves-light green btnAction' onClick=\"drawGroup.addToTable({id:'"+id+"'})\"><i class='material-icons'>add</i></button>");
    $('tr[data-id="' + id + '"]').remove();
  }
}
